extern crate ccl;

use std::fs::File;
use std::io::Read;

use ccl::compiler::config;
use ccl::compiler::util::print_iresult;

fn load_file_conents(file_name: &str) -> Vec<u8> {
    let mut file = match File::open(file_name) {
        Ok(file) => file,
        Err(e) => {
            println!("Failed to open file {} : {:?}", file_name, e);
            panic!();
        }
    };

    let mut buf = Vec::new();
    let _ = file.read_to_end(&mut buf);
    buf
}

#[test]
fn simple_config() {
    let contents = load_file_conents("tests/simple.ccl");
    let result = config::parse_config(&contents);

    print_iresult(&result, None);

    assert!(result.is_done());
}

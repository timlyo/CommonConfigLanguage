//! A project to compile any* configuration from a single input file
//!
//! *For a subset of any
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]


#![cfg_attr(test, feature(plugin))]
#![cfg_attr(test, plugin(quickcheck_macros))]

#[cfg(test)]
extern crate quickcheck;

pub mod compiler;

#[macro_use]
extern crate nom;

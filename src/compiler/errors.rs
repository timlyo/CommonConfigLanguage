//! Defines custom errors

use std::error::Error;
use std::fmt;

/// Enum of errors encountered by the compiler
#[derive(Debug, PartialEq, Clone)]
pub enum CompilerError {
    MissingError(MissingError),
    UnrecognisedError(UnrecognisedError),
    LogicError(LogicError),
    ParameterError(ParameterError),
}

impl CompilerError {
    //Convenience functions

    pub fn missing_error(what: &str, where_from: &str) -> CompilerError {
        CompilerError::MissingError(MissingError::new(what, where_from))
    }

    pub fn unrecognised_error(what: &str, where_from: &str) -> CompilerError {
        CompilerError::UnrecognisedError(UnrecognisedError::new(what, where_from))
    }

    pub fn logic_error(what: &str, where_from: &str) -> CompilerError {
        CompilerError::LogicError(LogicError::new(what, where_from))
    }

    pub fn parameter_error(what: &str, where_from: &str) -> CompilerError {
        CompilerError::ParameterError(ParameterError::new(what, where_from))
    }
}

/// Error for when something is missing from the input
///
/// E.g. a parameter is missing from a trait definition
#[derive(Debug, PartialEq, Clone)]
pub struct MissingError {
    /// What was missing
    pub what: String,
    /// Where is was missing from
    pub where_from: String,
}

impl MissingError {
    pub fn new(what: &str, where_from: &str) -> MissingError {
        MissingError {
            what: what.to_string(),
            where_from: where_from.to_string(),
        }
    }
}

impl Error for MissingError {
    fn description(&self) -> &str {
        "Something is missing" //TODO make more descriptive
    }

    fn cause(&self) -> Option<&Error> {
        None
    }
}

impl fmt::Display for MissingError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Missing {} from {}", self.what, self.where_from)
    }
}

/// Error for when input is not recognised
///
/// E.g. trait is defined with the wrong name
#[derive(Debug, PartialEq, Clone)]
pub struct UnrecognisedError {
    pub what: String,
    pub where_from: String,
}

impl UnrecognisedError {
    pub fn new(what: &str, where_from: &str) -> UnrecognisedError {
        UnrecognisedError {
            what: what.to_string(),
            where_from: where_from.to_string(),
        }
    }
}

impl Error for UnrecognisedError {
    fn description(&self) -> &str {
        "Something is unrecognised" //TODO make more descriptive
    }

    fn cause(&self) -> Option<&Error> {
        None
    }
}

impl fmt::Display for UnrecognisedError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Missing {} from {}", self.what, self.where_from)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct LogicError {
    pub what: String,
    pub where_from: String,
}

impl LogicError {
    pub fn new(what: &str, where_from: &str) -> LogicError {
        LogicError {
            what: what.to_string(),
            where_from: where_from.to_string(),
        }
    }
}

impl Error for LogicError {
    fn description(&self) -> &str {
        "Something is unrecognised" //TODO make more descriptive
    }

    fn cause(&self) -> Option<&Error> {
        None
    }
}

impl fmt::Display for LogicError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Missing {} from {}", self.what, self.where_from)
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct ParameterError {
    pub what: String,
    pub where_from: String,
}

impl ParameterError {
    pub fn new(what: &str, where_from: &str) -> ParameterError {
        ParameterError {
            what: what.to_string(),
            where_from: where_from.to_string(),
        }
    }
}

impl Error for ParameterError {
    fn description(&self) -> &str {
        "Something is unrecognised" //TODO make more descriptive
    }

    fn cause(&self) -> Option<&Error> {
        None
    }
}

impl fmt::Display for ParameterError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Missing {} from {}", self.what, self.where_from)
    }
}

use compiler::schema::Schema;
use compiler::schema::variables::parse_variable_list;
use compiler::schema::traits::parse_trait_list;
use nom::*;

/// Parse a schema and return a Schema object
named!(pub parse_schema <&[u8], Schema>,
    do_parse!(
        variables: parse_variable_list >>
        many0!(eol) >>
        traits: parse_trait_list >>

        (Schema{
            variables: variables,
            traits: traits,
        })
   )
);

#[cfg(test)]
mod test_schema {
    use super::*;
    use std::collections::HashMap;
    use compiler::schema::traits::{Trait, KeyValue, Blocks};
    use compiler::schema::variables::Property;
    use compiler::util::print_iresult;

    #[test]
    fn test_valid() {
        let input = "\
# Variables
variable:
    - property: something
cheese:
    - taste: tangy

# Traits
key_value(name_format='{name}', separator=':')
blocks(prefix='', name_format='{name}', start_delimiter='[', end_delimiter=']')"
                .as_bytes();

        let mut variables = HashMap::new();
        variables.insert("variable".into(),
                         vec![Property::new("property", "something")]);

        variables.insert("cheese".into(), vec![Property::new("taste", "tangy")]);

        let traits = vec![Trait::KeyValue(KeyValue {
                                              name_format: "{name}".to_string(),
                                              separator: ":".to_string(),
                                          }),
                          Trait::Blocks(Blocks::new("", "{name}", "[", "]"))];

        let expected = Schema {
            variables: variables,
            traits: traits,
        };

        let result = parse_schema(input);

        print_iresult(&result, Some(&expected));

        assert_eq!(result, IResult::Done("".as_bytes(), expected));
    }

    #[test]
    fn test_empty_variables() {
        let input = "\
# Variables

# Traits
key_value(separator=':', name_format='{name}')"
                .as_bytes();

        let variables = HashMap::new();
        let traits = vec![Trait::KeyValue(KeyValue {
                                              name_format: "{name}".to_string(),
                                              separator: ":".to_string(),
                                          })];

        let expected = Schema {
            variables: variables,
            traits: traits,
        };

        let result = parse_schema(input);

        print_iresult(&result, Some(&expected));

        assert_eq!(result, IResult::Done("".as_bytes(), expected));
    }
}

//! contains data a functionality for a schema object

use compiler::schema::variables::Property;
use std::collections::HashMap;
use compiler::schema::traits::*;

/// Stores all data for describing a schema
#[derive(Debug, PartialEq, Clone)]
pub struct Schema {
    pub traits: Vec<Trait>,
    pub variables: HashMap<String, Vec<Property>>,
}

impl Schema {
    /// Return this schema's key_value trait or `None` if one is not contained
    pub fn get_key_value_trait(&self) -> Option<&KeyValue> {
        for thing in &self.traits {
            match *thing {
                Trait::KeyValue(ref kv) => return Some(kv),
                _ => continue,
            }
        }
        None
    }

    /// Return this schema's block trait or `None` if one is not contained
    pub fn get_block_trait(&self) -> Option<&Blocks> {
        for thing in &self.traits {
            match *thing {
                Trait::Blocks(ref kv) => return Some(kv),
                _ => continue,
            }
        }
        None
    }

    /// Return this schema's block trait or `None` if one is not contained
    pub fn get_line_endings_trait(&self) -> Option<&LineEndings> {
        for thing in &self.traits {
            match *thing {
                Trait::LineEndings(ref kv) => return Some(kv),
                _ => continue,
            }
        }
        None
    }

    /// Return this schema's block trait or `None` if one is not contained
    pub fn get_lists_trait(&self) -> Option<&Lists> {
        for thing in &self.traits {
            match *thing {
                Trait::Lists(ref kv) => return Some(kv),
                _ => continue,
            }
        }
        None
    }
}

//! contains data structures and functionality for schema parsing and creation

pub mod parse;
pub mod traits;
pub mod variables;
pub mod schema;

pub use self::schema::Schema;
pub use self::parse::parse_schema;

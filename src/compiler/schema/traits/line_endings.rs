use compiler::schema::traits::NamedParameterList;
use compiler::errors::CompilerError;
use compiler::expressions::HasOutput;

#[derive(Debug, PartialEq, Clone)]
pub struct LineEndings {
    pub terminator: String,
    //pub post_block: bool // Whether to do after a block
}

impl LineEndings {
    pub fn from_named_parameters(parameters: &NamedParameterList)
                                 -> Result<LineEndings, CompilerError> {
        let terminator = match parameters.get_parameter_by_name("terminator".to_string()) {
            Some(prefix) => prefix,
            None => {
                return Err(CompilerError::missing_error("terminator parameter",
                                                        "line_endings definition"))
            }
        };

        Ok(LineEndings { terminator: terminator.value.get_output() })
    }
}

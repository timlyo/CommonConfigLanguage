use compiler::schema::traits::NamedParameterList;
use compiler::errors::CompilerError;
use compiler::expressions::HasOutput;

#[derive(Debug, PartialEq, Clone)]
pub struct Lists {
    pub start_delimiter: String,
    pub end_delimiter: String,
    pub separator: String,
}

impl Lists {
    pub fn from_named_parameters(parameters: &NamedParameterList) -> Result<Lists, CompilerError> {
        let start_delimiter =
            match parameters.get_parameter_by_name("start_delimiter".to_string()) {
                Some(prefix) => prefix,
                None => {
                    return Err(CompilerError::missing_error("start_delimiter parameter",
                                                            "lists definition"))
                }
            };

        let end_delimiter = match parameters.get_parameter_by_name("end_delimiter".to_string()) {
            Some(prefix) => prefix,
            None => {
                return Err(CompilerError::missing_error("end_delimiter parameter",
                                                        "lists definition"))
            }
        };

        let separator = match parameters.get_parameter_by_name("separator".to_string()) {
            Some(prefix) => prefix,
            None => {
                return Err(CompilerError::missing_error("separator parameter", "lists definition"))
            }
        };

        Ok(Lists {
               start_delimiter: start_delimiter.value.get_output(),
               end_delimiter: end_delimiter.value.get_output(),
               separator: separator.value.get_output(),
           })
    }
}

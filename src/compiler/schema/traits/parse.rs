use nom::*;
use std::str;

use compiler::expressions::{parse_expression, Expression};
use compiler::general::identifier;
use compiler::errors::{CompilerError, UnrecognisedError};
use compiler::schema::traits::*;

named!(pub parse_trait_list<&[u8], Vec<Trait>>,
   do_parse!(
        add_return_error!(ErrorKind::Custom(104), tag!("# Traits")) >> eol >>

        traits: 
            many0!(ws!(
                   parse_trait
                )) >>

        (traits)
    )
);


#[cfg(test)]
mod test_parse_trait_list {
    use super::*;
    use compiler::util::print_iresult;

    #[test]
    fn test_valid() {
        let input = "\
        # Traits
        key_value(separator=':', name_format='{name}')"
                .as_bytes();

        let expected = vec![Trait::KeyValue(KeyValue {
                                                name_format: "{name}".to_string(),
                                                separator: ":".to_string(),
                                            })];

        let result = parse_trait_list(input);

        print_iresult(&result, Some(&expected));

        assert_eq!(result, IResult::Done("".as_bytes(), expected));

    }
}

/// Creates a `Trait` from a name and a `NamedParameterList`
///
/// # Panics
/// Panics if the trait is missing a required parameter from list
fn get_trait(name: String, parameters: NamedParameterList) -> Result<Trait, CompilerError> {
    let captured_trait = match name.as_str() {
        "key_value" => Trait::KeyValue(try!(KeyValue::from_named_parameters(&parameters))),
        "blocks" => Trait::Blocks(try!(Blocks::from_named_parameters(&parameters))),
        "line_endings" => Trait::LineEndings(try!(LineEndings::from_named_parameters(&parameters))),
        "lists" => Trait::Lists(try!(Lists::from_named_parameters(&parameters))),
        _ => {
            return Err(CompilerError::UnrecognisedError(UnrecognisedError {
                                                            what: name,
                                                            where_from: "parsing trait list"
                                                                .to_string(),
                                                        }))
        }
    };

    Ok(captured_trait)
}

named!(pub parse_trait<&[u8], Trait>,
    do_parse!(
        name: identifier >>
        parameters: parse_parameters >>

        (match get_trait(name, parameters){
            Ok(t) => t,
            Err(e) => match e {
                CompilerError::MissingError(e) => return IResult::Error(ErrorKind::Custom(201)),
                CompilerError::UnrecognisedError(e) => return IResult::Error(ErrorKind::Custom(202)),
                _ => {
                    // Nothing else should come from trait parsing
                    panic!()
                }
            }
        })
    )
);

named!(parse_parameters<&[u8], NamedParameterList>,
    do_parse!(
        parameters: delimited!(
            tag!("("),
            separated_list!(tag!(","), parse_named_parameter),
            tag!(")")
        ) >>

        (NamedParameterList{
            parameters: parameters
        })
    )
);

#[cfg(test)]
mod test_parse_trait {
    use super::*;

    #[test]
    fn test_valid_key_value() {
        assert_eq!(parse_trait("key_value(separator=':', name_format='{name}')".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Trait::KeyValue(KeyValue {
                                                     name_format: "{name}".to_string(),
                                                     separator: ":".to_string(),
                                                 })));
    }

    #[test]
    fn test_unrecognised() {
        let result = parse_trait("blurp(prefix='', end_delimiter=']')".as_bytes());

        assert_eq!(result, IResult::Error(ErrorKind::Custom(202)));
    }

    #[test]
    fn test_empty() {
        let result = parse_trait("key_value()".as_bytes());

        assert_eq!(result, IResult::Error(ErrorKind::Custom(201)));
    }

    #[test]
    fn test_missing() {
        let result = parse_trait("blocks(prefix='', end_delimiter=']')".as_bytes());

        assert_eq!(result, IResult::Error(ErrorKind::Custom(201)));
    }

    #[test]
    fn test_valid_blocks() {
        let input = "blocks(prefix='', name_format='{name}', start_delimiter='[', end_delimiter=']')"
            .as_bytes();
        assert_eq!(parse_trait(input),
                   IResult::Done("".as_bytes(),
                                 Trait::Blocks(Blocks::new("", "{name}", "[", "]"))));
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct NamedParameterList {
    pub parameters: Vec<NamedParameter>,
}

impl NamedParameterList {
    /// Search through a parameter list by name to return a single named parameter
    pub fn get_parameter_by_name(&self, name: String) -> Option<&NamedParameter> {
        for param in &self.parameters {
            if param.name == name {
                return Some(param);
            }
        }

        None
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct NamedParameter {
    pub name: String,
    pub value: Expression,
}

impl NamedParameter {
    pub fn new(name: &str, value: Expression) -> NamedParameter {
        NamedParameter {
            name: name.to_string(),
            value: value,
        }
    }

    pub fn from_str(name: &str, value: &str) -> Result<NamedParameter, ErrorKind> {
        let value = match parse_expression(value.as_bytes()).to_result() {
            Ok(value) => value,
            Err(e) => return Err(e),
        };

        Ok(NamedParameter::new(name, value))
    }
}

named!(pub parse_named_parameter<&[u8], NamedParameter>,
   ws!(do_parse!(
       name: identifier >>
       tag!("=") >>
       value: parse_expression >>

       (NamedParameter {
           name: name,
           value: value
       })
    ))
);

#[cfg(test)]
mod test_named_parameter {
    use super::*;

    #[test]
    fn test_valid() {
        assert_eq!(parse_named_parameter("a='b'".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 NamedParameter::new("a", Expression::string("b"))));
    }

    #[test]
    fn test_whitespace() {
        assert_eq!(parse_named_parameter(" a = 'b' ".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 NamedParameter::new("a", Expression::string("b"))));
    }
}

mod key_value;
mod blocks;
mod line_endings;
mod lists;
mod comments;
mod parse;

pub use self::key_value::KeyValue;
pub use self::blocks::Blocks;
pub use self::line_endings::LineEndings;
pub use self::lists::Lists;
pub use self::comments::Comments;
pub use self::parse::{parse_trait_list, NamedParameter, NamedParameterList};

#[derive(Debug, PartialEq, Clone)]
pub enum Trait {
    KeyValue(KeyValue),
    Blocks(Blocks),
    LineEndings(LineEndings),
    Lists(Lists),
    Comments(Comments),
}

use compiler::schema::traits::NamedParameterList;
use compiler::errors::{CompilerError, MissingError};
use compiler::expressions::HasOutput;

#[derive(Debug, PartialEq, Clone)]
pub struct KeyValue {
    pub separator: String,
    pub name_format: String,
}

impl KeyValue {
    pub fn from_named_parameters(parameters: &NamedParameterList)
                                 -> Result<KeyValue, CompilerError> {
        let separator = match parameters.get_parameter_by_name("separator".to_string()) {
            Some(separator) => separator,
            None => {
                return Err(CompilerError::MissingError(MissingError::new("separator parameter",
                                                                         "key value definition")))
            }
        };

        let name_format = match parameters.get_parameter_by_name("name_format".to_string()) {
            Some(separator) => separator,
            None => {
                return Err(CompilerError::MissingError(MissingError::new("name_format parameter",
                                                                         "key value definition")))
            }
        };

        Ok(KeyValue {
               name_format: name_format.value.get_output(),
               separator: separator.value.get_output(),
           })
    }
}


#[cfg(test)]
mod test_key_value {
    use super::*;
    use compiler::schema::traits::NamedParameter;

    #[test]
    fn test_from_named_parameters() {
        let parameters = NamedParameterList {
            parameters: vec![NamedParameter::from_str("separator", "':'").unwrap(),
                             NamedParameter::from_str("name_format", "'{name}'").unwrap()],
        };

        let key_value = KeyValue::from_named_parameters(&parameters).unwrap();
        assert_eq!(key_value,
                   KeyValue {
                       name_format: "{name}".to_string(),
                       separator: ":".to_string(),
                   })
    }
}

use compiler::schema::traits::NamedParameterList;
use compiler::errors::CompilerError;
use compiler::expressions::HasOutput;

#[derive(Debug, PartialEq, Clone)]
pub struct Blocks {
    pub prefix: String,
    pub name_format: String,
    pub start_delimiter: String,
    pub end_delimiter: String,
}

impl Blocks {
    pub fn new(prefix: &str,
               name_format: &str,
               start_delimiter: &str,
               end_delimiter: &str)
               -> Blocks {
        Blocks {
            prefix: prefix.to_string(),
            name_format: name_format.to_string(),
            start_delimiter: start_delimiter.to_string(),
            end_delimiter: end_delimiter.to_string(),
        }
    }

    pub fn from_named_parameters(parameters: &NamedParameterList) -> Result<Blocks, CompilerError> {
        let prefix = match parameters.get_parameter_by_name("prefix".to_string()) {
            Some(prefix) => prefix,
            None => {
                return Err(CompilerError::missing_error("prefix parameter", "blocks definition"))
            }
        };

        let name_format = match parameters.get_parameter_by_name("name_format".to_string()) {
            Some(prefix) => prefix,
            None => {
                return Err(CompilerError::missing_error("name_format parameter",
                                                        "blocks definition"))
            }
        };

        let start = match parameters.get_parameter_by_name("start_delimiter".to_string()) {
            Some(start) => start,
            None => {
                return Err(CompilerError::missing_error("start_delimiter parameter",
                                                        "blocks definition"))
            }
        };

        let end = match parameters.get_parameter_by_name("end_delimiter".to_string()) {
            Some(end) => end,
            None => {
                return Err(CompilerError::missing_error("end_delimiter parameter",
                                                        "blocks definition"))
            }
        };


        Ok(Blocks {
               prefix: prefix.value.get_output(),
               name_format: name_format.value.get_output(),
               start_delimiter: start.value.get_output(),
               end_delimiter: end.value.get_output(),
           })
    }
}

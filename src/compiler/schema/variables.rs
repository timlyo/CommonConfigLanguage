//! Stores the list of required variables for a schema
//!
//! # Format
//! ```ccl
//! # Variables
//! name:
//!     - property1: value
//!     - property2: value
//! ```

use nom::*;
use std::str;
use std::collections::HashMap;

/// Parse the entire variable section including header
named!(pub parse_variable_list <&[u8], HashMap<String, Vec<Property>>>,
    do_parse!(
        add_return_error!(ErrorKind::Custom(103), tag!("# Variables")) >> eol >>
        variables:
            many0!(parse_variable) >>

        (
            variables.into_iter().map(|v| v.to_tuple()).collect()
        )
    )
);

#[cfg(test)]
mod test_variable_list {
    use super::*;
    use compiler::util::print_iresult;

    #[test]
    fn test_valid() {
        let input = "# Variables
variable:
    - thing: yes
variable2:
    - perhaps: maybe"
                .as_bytes();

        let mut expected = HashMap::new();
        expected.insert("variable".to_string(), vec![Property::new("thing", "yes")]);
        expected.insert("variable2".to_string(),
                        vec![Property::new("perhaps", "maybe")]);

        let result = parse_variable_list(input);

        print_iresult(&result, Some(&expected));

        assert_eq!(result, IResult::Done("".as_bytes(), expected));


    }

    #[test]
    fn test_empty() {
        let input = "# Variables
"
                .as_bytes();

        let expected = HashMap::new();

        let result = parse_variable_list(input);

        print_iresult(&result, Some(&expected));

        assert_eq!(result, IResult::Done("".as_bytes(), expected));


    }

}

/// Struct for a single variable
#[derive(Debug, PartialEq)]
pub struct Variable {
    pub name: String,
    /// List of properties that the variable can have
    pub properties: Vec<Property>,
}

impl Variable {
    pub fn to_tuple(&self) -> (String, Vec<Property>) {
        (self.name.clone(), self.properties.clone())
    }
}

/// Parse a single variable definition
named!(pub parse_variable <&[u8], Variable>, 
    do_parse!(
        name:
            map_res!(
                take_while_s!(is_alphanumeric), 
                str::from_utf8
            ) >> 
            tag!(":") >> 
            eol >>

        properties: 
            many0!(parse_property) >>

        (Variable{
            name: name.to_string(),
            properties: properties
        })
    )
);

#[cfg(test)]
mod test_variable {
    use super::*;
    use compiler::util::print_iresult;

    #[test]
    fn test_valid() {
        let input = "variable:
                        - property:false
                        - thing:maybe"
                .as_bytes();

        let expected = Variable {
            name: "variable".to_string(),
            properties: vec![Property {
                                 name: "property".to_string(),
                                 value: "false".to_string(),
                             },
                             Property {
                                 name: "thing".to_string(),
                                 value: "maybe".to_string(),
                             }],
        };

        let result = parse_variable(input);
        print_iresult(&result, Some(&expected));

        assert_eq!(result, IResult::Done("".as_bytes(), expected));
    }
}

/// Struct for a variable property
#[derive(Debug, PartialEq, Clone)]
pub struct Property {
    name: String,
    value: String,
}

impl Property {
    pub fn new(name: &str, value: &str) -> Property {
        Property {
            name: name.to_string(),
            value: value.to_string(),
        }
    }
}

named!(parse_property <&[u8], Property>, 
    do_parse!(
        // indent and dash 
        add_return_error!(ErrorKind::Custom(101), multispace) >> 
        add_return_error!(ErrorKind::Custom(100), tag!("-")) >> 
        opt!(tag!(" ")) >>

        name: 
            map_res!(take_while!(is_alphanumeric), str::from_utf8) >> 

        opt!(multispace) >> 
        tag!(":") >> 
        opt!(multispace) >>

        value: 
            map_res!(
                take_while!(is_alphanumeric), 
                str::from_utf8
            ) >>

        many0!(eol) >>

       (Property{
           name: name.to_string(),
           value: value.to_string()
       })
    )
);

#[cfg(test)]
mod test_property {
    use super::*;

    #[test]
    fn test_valid() {
        assert_eq!(parse_property("\t-thing:thong\n".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Property {
                                     name: "thing".to_string(),
                                     value: "thong".to_string(),
                                 }))
    }

    #[test]
    fn test_indent() {
        assert_eq!(parse_property("                \t              -thing:thong".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Property {
                                     name: "thing".to_string(),
                                     value: "thong".to_string(),
                                 }))
    }

    #[test]
    fn test_spaces() {
        assert_eq!(parse_property("\t- thing : thong\n".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Property {
                                     name: "thing".to_string(),
                                     value: "thong".to_string(),
                                 }))
    }

    #[test]
    fn test_no_indent_error() {
        assert_eq!(parse_property("-thing:thong".as_bytes()),
                   IResult::Error(ErrorKind::Custom(101)))
    }

    #[test]
    fn test_no_dash_error() {
        assert_eq!(parse_property("\tthing:thong".as_bytes()),
                   IResult::Error(ErrorKind::Custom(100)))
    }
}

use nom::IResult;
use std::fmt::Debug;

pub fn print_iresult<O: Debug>(result: &IResult<&[u8], O>, expected: Option<&O>) {
    match result {
        &IResult::Done(i, ref o) => {
            println!("Input: {:#?}", i);
            println!("Output: {:#?}", o)
        }
        _ => println!("Result: {:?}", result),
    };

    if let Some(expected) = expected {
        println!("Expected: {:#?}", expected);
    }
}

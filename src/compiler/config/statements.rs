//! structs for storing statements found in a config

use compiler::expressions::{Expression, parse_expression};
use std::str;
use std::fmt;
use compiler::general::identifier;
use nom;

/// Enum to store all statement types
#[derive(Debug, PartialEq, Clone)]
pub enum Statement {
    Expression(Expression),
    Constant(Constant),
}

named_attr!(
    #[doc = "Parse a statement, Relies in on underlying parsers"],
pub statement <&[u8], Statement>,
    alt_complete!(
        constant => {|i| Statement::Constant(i)} |
        parse_expression => {|i| Statement::Expression(i)}
    )
);


#[cfg(test)]
mod test_statement {
    use super::*;
    use nom::*;
    use compiler::util::print_iresult;
    use compiler::config::statements::Constant;
    use compiler::expressions;
    use compiler::expressions::Block;

    #[test]
    fn constant() {
        let result = Statement::Constant(Constant {
                                             name: "x".to_string(),
                                             value: Expression::integer(10),
                                         });

        assert_eq!(statement("let x = 10".as_bytes()),
                   IResult::Done("".as_bytes(), result))
    }

    #[test]
    fn expression() {
        let result = Statement::Expression(Expression::float(9.25));

        assert_eq!(statement("9.25".as_bytes()),
                   IResult::Done("".as_bytes(), result))
    }


    #[test]
    fn block() {
        let expected =
            Statement::Expression(Expression::Block(Block {
                                                        name: "block".to_string(),
                                                        body: vec![Statement::Constant(Constant{
                name: "x".to_string(),
                value: expressions::parse_expression("[1, 2]".as_bytes()).unwrap().1
            })],
                                                    }));

        let input = "\
block{
    let x = [1, 2]
}"
                .as_bytes();

        let result = statement(input);

        print_iresult(&result, Some(&expected));
        assert_eq!(result, IResult::Done("".as_bytes(), expected))
    }
}

/// A config constant `let x = 10`
#[derive(PartialEq, Clone)]
pub struct Constant {
    /// Constant name
    pub name: String,
    /// Constant value
    pub value: Expression,
}

impl Constant {
    /// Create a new constant from the str and value that are passed into it
    pub fn new(name: &str, value: Expression) -> Constant {
        Constant {
            name: name.to_string(),
            value: value,
        }
    }

    /// Create a new constant, but evaluate the string with the expression parser and set value
    /// equal to the result
    ///
    /// Returns Err if value cannot be parsed as an expression
    pub fn from_str(name: &str, value: &str) -> Result<Constant, nom::Err> {
        let value = match parse_expression(value.as_bytes()).to_result() {
            Ok(value) => value,
            Err(e) => return Err(e),
        };

        Ok(Constant::new(name, value))
    }
}

impl fmt::Debug for Constant {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Constant({} = {:?})", self.name, self.value)
    }
}

/// Parse a constant
named_attr!(
    #[doc = "Parse a constant"],
pub constant <&[u8], Constant>,
    ws!(do_parse!(
            tag!("let") >>
            name: identifier >>
            tag!("=") >>
            value: parse_expression >>

            (Constant{
                name: name,
                value: value
            })
        )
    )
);

#[cfg(test)]
mod test_constant {
    use super::*;
    use nom::*;
    use compiler::expressions::*;

    #[test]
    fn valid() {
        assert_eq!(constant("let x=1".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Constant {
                                     name: "x".to_string(),
                                     value: Expression::integer(1),
                                 }))
    }

    #[test]
    fn spacing() {
        assert_eq!(constant("let x = 1".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Constant {
                                     name: "x".to_string(),
                                     value: Expression::integer(1),
                                 }))
    }
}

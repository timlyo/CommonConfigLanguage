//! Contains modules pertaining to the input config

pub mod parse;
pub mod statements;
pub mod config;

pub use self::parse::parse_config;

//! Internal functions and structs for representing a config

use compiler::config::statements::Statement;
use compiler::expressions::Expression;
use std::collections::HashMap;
use std::iter::Extend;

/// Data relevant to a single configuration
pub struct Config {
    /// Map of constants in a config
    constants: HashMap<String, Expression>,
}

impl Config {
    /// Create a config struct from the output of the config parser
    pub fn from_ast(&self, ast: Vec<Statement>) -> Config {
        Config { constants: extract_constants(&ast) }
    }
}

/// Create a hashmap from the constants found in an ast
fn extract_constants(ast: &Vec<Statement>) -> HashMap<String, Expression> {
    let mut constants = HashMap::new();
    for statement in ast {
        match *statement {
            Statement::Constant(ref constant) => {
                constants.insert(constant.name.clone(), constant.value.clone());
            }
            Statement::Expression(ref expression) => {
                match *expression {
                    Expression::Block(ref block) => {
                        constants.extend(extract_constants(&block.body))
                    }
                    _ => {}
                }
            }
        }
    }

    constants
}
#[cfg(test)]
mod test_constant {
    use super::*;
    use compiler::config::parse::parse_config;

    #[test]
    fn test_extract_constants() {
        let input = "let x = 10
        thing {
            let y = 20
        }"
                .as_bytes();

        let config = parse_config(input).unwrap().1;
        let constants = extract_constants(&config);

        let mut expected = HashMap::new();
        expected.insert("x".to_string(), Expression::integer(10));
        expected.insert("y".to_string(), Expression::integer(20));

        assert_eq!(constants, expected);
    }
}

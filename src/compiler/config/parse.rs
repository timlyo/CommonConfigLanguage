//! Top level functions for parsing a configuration

use compiler::config::statements::{Statement, statement};
use compiler::expressions::{Expression, Variable};
use std::boxed::Box;

/// Get the result of parsing a config as an ast
named!(pub parse_config <&[u8], Vec<Statement>>, 
    many0!(
        ws!(statement)
    )
);


#[cfg(test)]
mod test_parse {
    use super::*;
    use compiler::expressions::*;
    use compiler::config::statements::Constant;
    use nom::IResult;
    use compiler::util::print_iresult;

    #[test]
    fn one() {
        let input = "\
version = '0.1'
thing = 'thing'

block {
    x = [1, 2]
}";

        let variable = Statement::Expression(Expression::Variable(Variable{
                                                 name: "version".to_string(),
                                                 value: Box::new(Expression::string("0.1")),
                                             }));
        let variable2 = Statement::Expression(Expression::Variable(Variable{
                                                 name: "thing".to_string(),
                                                 value: Box::new(Expression::string("thing")),
                                             }));
        

        let block = Statement::Expression(Expression::Block(Block {
                                                                name: "block".to_string(),
                                                                body: vec![Statement::Expression(Expression::Variable(Variable {
                           name: "x".to_string(),
                           value: Box::new(Expression::List(List {
                               data: vec![Expression::integer(1),
                                          Expression::integer(2)],
                           })),
                       }))],
                                                            }));

        let expected = vec![variable, variable2, block];

        let result = parse_config(input.as_bytes());

        print_iresult(&result, Some(&expected));

        assert_eq!(result, IResult::Done("".as_bytes(), expected))
    }
}

use compiler::expressions::{parse_expression, Expression};
use compiler::general::identifier;
use std::str;

/// e.g. `do_thing(param)`
#[derive(Debug, PartialEq, Clone)]
pub struct Function {
    pub name: String,
    pub parameters: Vec<Expression>,
}

named!(pub parse_function<&[u8], Function>,
    ws!(do_parse!(
        name: identifier >>
        parameters: delimited!(
                tag!("("),
                dbg!(ws!(separated_list!(tag!(","), parse_expression))),
                tag!(")")
            )>>
        (Function{
            name: name.to_string(),
            parameters: parameters
        })
    ))
);

#[cfg(test)]
mod test_function {
    use super::*;
    use nom::*;

    #[test]
    fn valid() {
        assert_eq!(parse_function("func()".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Function {
                                     name: "func".to_string(),
                                     parameters: Vec::new(),
                                 }));
    }

    #[test]
    fn alphanumeric() {
        assert_eq!(parse_function("f1u2n3c4()".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Function {
                                     name: "f1u2n3c4".to_string(),
                                     parameters: Vec::new(),
                                 }));
    }

    #[test]
    fn spaces() {
        assert_eq!(parse_function("func ( )".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Function {
                                     name: "func".to_string(),
                                     parameters: Vec::new(),
                                 }));
    }

    #[test]
    fn with_underscores() {
        assert_eq!(parse_function("func_test ( )".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Function {
                                     name: "func_test".to_string(),
                                     parameters: Vec::new(),
                                 }));
    }

    #[test]
    fn single_parameter() {
        assert_eq!(parse_function("func_test(10)".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Function {
                                     name: "func_test".to_string(),
                                     parameters: vec![Expression::integer(10)],
                                 }));
    }

    #[test]
    fn parameters() {
        assert_eq!(parse_function("func_test(10, 'test')".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Function {
                                     name: "func_test".to_string(),
                                     parameters: vec![Expression::integer(10),
                                                      Expression::string("test")],
                                 }));
    }

    #[test]
    /// Check that a function name starting with a number is invalid
    fn alpha_start() {
        assert_eq!(parse_function("0test()".as_bytes()),
                   IResult::Error(ErrorKind::Verify));
    }
}

use compiler::expressions::{parse_expression, Expression};
use std::boxed::Box;
use std::fmt;
use nom::*;
use compiler::general::identifier;

/// e.g. `x=10`
///
/// Different to a constant, can be used as output
#[derive(Debug, PartialEq, Clone)]
pub struct Variable {
    pub name: String,
    pub value: Box<Expression>,
}

impl Variable {
    pub fn new(name: &str, value: Expression) -> Variable {
        Variable {
            name: name.to_string(),
            value: Box::new(value),
        }
    }

    pub fn from_str(name: &str, value: &str) -> Result<Variable, ErrorKind> {
        let value = match parse_expression(value.as_bytes()).to_result() {
            Ok(value) => value,
            Err(e) => return Err(e),
        };

        Ok(Variable::new(name, value))
    }
}

impl fmt::Display for Variable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}={:?}", self.name, self.value)
    }
}

named!(pub parse_variable <&[u8], Variable>,
    do_parse!(
        name: identifier >>
        ws!(tag!("=")) >>
        value: parse_expression >>
    
        (Variable::new(&name, value))
    )
);

#[cfg(test)]
mod test_variable {
    use super::*;

    #[test]
    fn valid() {
        assert_eq!(parse_variable("x=10".as_bytes()),
                   IResult::Done("".as_bytes(), Variable::from_str("x", "10").unwrap()))
    }

    #[test]
    fn new_line_consumption() {
        assert_eq!(parse_variable("x='thing'\n".as_bytes()),
                   IResult::Done("\n".as_bytes(), Variable::from_str("x", "'thing'").unwrap()))
    }

    #[test]
    fn multicharacter() {
        assert_eq!(parse_variable("mahvariable=4.0".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Variable::from_str("mahvariable", "4.0").unwrap()))
    }
}

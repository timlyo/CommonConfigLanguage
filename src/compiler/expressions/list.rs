use compiler::expressions::{parse_expression, Expression};
use std::fmt;

/// e.g. `[1, 2, 3.0]`
#[derive(PartialEq, Clone)]
pub struct List {
    pub data: Vec<Expression>,
}

impl List {
    fn new() -> List {
        List { data: Vec::new() }
    }

    pub fn first(&self) -> Option<&Expression> {
        self.data.first()
    }
}

named!(pub parse_list<&[u8], List>, 
    do_parse!(
        data: ws!(
            delimited!(
                tag!("["),
                separated_list!(tag!(","), parse_expression),
                tag!("]")
            )
        ) >>
        (List{data: data})
    )
);


impl fmt::Debug for List {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "List({:?})", self.data)
    }
}


#[cfg(test)]
mod test_list {
    use super::*;
    use nom::*;

    #[test]
    fn test_empty() {
        assert_eq!(parse_list("[]".as_bytes()),
                   IResult::Done("".as_bytes(), List::new()));
    }


    #[test]
    fn test_integers() {
        let result = List {
            data: vec![Expression::integer(1),
                       Expression::integer(2),
                       Expression::integer(3)],
        };

        assert_eq!(parse_list("[1,2,3]".as_bytes()),
                   IResult::Done("".as_bytes(), result))
    }

    #[test]
    fn test_whitespace() {
        let result = List {
            data: vec![Expression::integer(1),
                       Expression::integer(2),
                       Expression::integer(3)],
        };

        assert_eq!(parse_list("[ 1, 2, 3]".as_bytes()),
                   IResult::Done("".as_bytes(), result));
    }

    #[test]
    fn test_mixed() {
        let result = List {
            data: vec![Expression::integer(1),
                       Expression::float(2.0),
                       Expression::string("3")],
        };

        assert_eq!(parse_list("[1, 2.0, '3']".as_bytes()),
                   IResult::Done("".as_bytes(), result));
    }

    #[test]
    fn list_of_lists() {
        assert_eq!(parse_list("[[1, 2], [3, 4]]".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 List {
                                     data: vec![parse_expression("[1, 2]".as_bytes())
                                                    .to_result()
                                                    .unwrap(),
                                                parse_expression("[3, 4]".as_bytes())
                                                    .to_result()
                                                    .unwrap()],
                                 }))
    }
}

use nom::*;
use std::str;

// TODO make nice and pretty
named!(pub parse_float<&[u8], f64>,
    do_parse!(
        sign: opt!(tag!("-")) >>
        integer: map_res!(digit, str::from_utf8) >>
        tag!(".") >>
        float: map_res!(digit, str::from_utf8) >>

        (
            format!("{}.{}", integer, float).parse::<f64>().unwrap() * match sign{
                Some(_) => -1.0,
                None => 1.0,
            }
        )
    )
);

#[cfg(test)]
mod test_float {
    use super::*;

    #[quickcheck]
    fn check_f64(i: f64) -> bool {
        let input = i.to_string();
        parse_float(input.as_bytes()) == IResult::Done("".as_bytes(), i)
    }

    #[test]
    fn test_invalid() {
        assert_eq!(parse_float("'String'".as_bytes()),
                   IResult::Error(ErrorKind::Digit))
    }
}

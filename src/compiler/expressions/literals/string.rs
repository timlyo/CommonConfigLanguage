use std::str;

named!(pub parse_string<&[u8], &str>, 
    complete!(alt!(
        delimited!(
            tag!("\""),
            map_res!(take_until!("\""), str::from_utf8),
            tag!("\"")
        ) |
        delimited!(
            tag!("'"),
            map_res!(take_until!("'"), str::from_utf8),
            tag!("'")
        )
    ))
);

#[cfg(test)]
mod test_string {
    use super::*;
    use quickcheck::TestResult;
    use nom::*;

    #[test]
    fn escapes() {
        assert_eq!(parse_string("'\n'".as_bytes()),
                   IResult::Done("".as_bytes(), "\u{000A}"))
    }

    #[quickcheck]
    fn double_quotes(input: String) -> TestResult {
        if input.contains("\"") {
            return TestResult::discard();
        }

        let wrapped = format!("\"{}\"", input);

        let result = parse_string(wrapped.as_bytes());

        TestResult::from_bool(result == IResult::Done("".as_bytes(), &input))
    }

    #[quickcheck]
    fn single_quotes(input: String) -> TestResult {
        if input.contains("'") {
            return TestResult::discard();
        }

        let wrapped = format!("'{}'", input);

        let result = parse_string(wrapped.as_bytes());

        TestResult::from_bool(result == IResult::Done("".as_bytes(), &input))
    }
}

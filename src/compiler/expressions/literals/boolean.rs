use std::str;
use std::str::FromStr;

named!(pub parse_boolean<&[u8], bool>,
        map_res!(
            map_res!(
                alt!(tag!("true") | tag!("false")),
                str::from_utf8
            ),
            FromStr::from_str
        ) 
    );

#[cfg(test)]
mod test_bool {
    use super::*;
    use nom::*;
    use quickcheck::TestResult;

    #[test]
    fn test_true() {
        assert_eq!(parse_boolean("true".as_bytes()),
                   IResult::Done("".as_bytes(), true));
    }

    #[test]
    fn test_false() {
        assert_eq!(parse_boolean("false".as_bytes()),
                   IResult::Done("".as_bytes(), false));
    }

    #[quickcheck]
    fn test_invalid(input: String) -> TestResult {
        if input == "true" || input == "false" {
            return TestResult::discard();
        }

        TestResult::from_bool(parse_boolean("ajsfbas".as_bytes()) == IResult::Error(ErrorKind::Alt))
    }
}

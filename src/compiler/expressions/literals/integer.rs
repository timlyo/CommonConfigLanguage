use nom::*;
use std::str;
use std::str::FromStr;

named!(pub parse_integer<&[u8], i64>,
    do_parse!(
        sign: opt!(tag!("-")) >>
        value: 
            map_res!(
                map_res!(
                    digit,
                    str::from_utf8
                ),
                FromStr::from_str
            )
        >>

        (match sign {
            Some(_) => value * -1,
            None => value
        })
    )
);

#[cfg(test)]
mod test_integer {
    use super::*;

    #[quickcheck]
    fn check_u64(i: u64) -> bool {
        let input = i.to_string();
        parse_integer(input.as_bytes()) == IResult::Done("".as_bytes(), i as i64)
    }

    #[quickcheck]
    fn check_i64(i: i64) -> bool {
        let input = i.to_string();
        parse_integer(input.as_bytes()) == IResult::Done("".as_bytes(), i)
    }

    #[test]
    fn string() {
        assert_eq!(parse_integer("Double-0-seven".as_bytes()),
                   IResult::Error(ErrorKind::Digit));
    }
}

//! basic datatypes of the language

use std::str;
use std::fmt;

mod boolean;
mod float;
mod integer;
mod string;


use self::boolean::parse_boolean;
use self::float::parse_float;
use self::integer::parse_integer;
use self::string::parse_string;

/// All possible literal types
#[derive(Debug, PartialEq, Clone)]
pub enum Literal {
    String(String),
    Integer(i64),
    Float(f64),
    Boolean(bool),
}

impl fmt::Display for Literal {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Literal::String(ref string) => write!(f, "{}", string),
            Literal::Integer(integer) => write!(f, "{}", integer),
            Literal::Float(float) => write!(f, "{}", float),
            Literal::Boolean(boolean) => write!(f, "{}", boolean),
        }
    }
}

named!(pub parse_literal<&[u8], Literal>,
    alt_complete!(
        parse_string  => { |i: &str| Literal::String(i.to_string()) } |
        parse_float   => { |i: f64 | Literal::Float(i) } |
        parse_integer => { |i: i64 | Literal::Integer(i) } |
        parse_boolean => { |i: bool| Literal::Boolean(i) } 
    )
);

#[cfg(test)]
mod test_literal {
    use super::*;
    use nom::*;
    use quickcheck::TestResult;

    // These tests are to check the correct type is returned, not the underlying parser

    #[test]
    fn string() {
        assert_eq!(parse_literal("'string'".as_bytes()),
                   IResult::Done("".as_bytes(), Literal::String("string".to_string())));
    }

    #[quickcheck]
    fn integer(integer: i64) {
        assert_eq!(parse_literal(integer.to_string().as_bytes()),
                   IResult::Done("".as_bytes(), Literal::Integer(integer)));
    }

    #[quickcheck]
    fn float(float: f64) -> TestResult {
        // All numbers with decimal place should be parsed as integers
        if float.to_string().contains(".") == false {
            return TestResult::discard();
        }

        let input = float.to_string();
        let result = parse_literal(input.as_bytes());
        println!("input {:?}", float.to_string());
        println!("Result {:?}", result);

        TestResult::from_bool(result == IResult::Done("".as_bytes(), Literal::Float(float)))
    }

    #[test]
    fn boolean() {
        assert_eq!(parse_literal("true".as_bytes()),
                   IResult::Done("".as_bytes(), Literal::Boolean(true)))
    }
}

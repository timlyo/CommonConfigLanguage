use compiler::config::statements::{statement, Statement};
use compiler::general::identifier;


/// e.g. `block {}`
#[derive(Debug, PartialEq, Clone)]
pub struct Block {
    pub name: String,
    pub body: Vec<Statement>,
}

named!(pub parse_block<&[u8], Block>,
    do_parse!(
        name:
            ws!(identifier) >>

        ws!(tag!("{")) >>
        
        body: 
            many0!(ws!(statement)) >>

        ws!(tag!("}")) >>

        (Block { name:name, body:body})
    )
);

#[cfg(test)]
mod test_block {
    use super::*;
    use nom::*;
    use compiler::util::print_iresult;
    use compiler::expressions::*;

    #[test]
    fn empty() {
        let input = "test{}".as_bytes();
        let result = parse_block(input);
        let expected = Block {
            name: "test".to_string(),
            body: Vec::new(),
        };

        print_iresult(&result, Some(&expected));
        assert_eq!(result, IResult::Done("".as_bytes(), expected))
    }

    #[test]
    fn single_statement() {
        let input = "test{20}".as_bytes();
        let result = parse_block(input);
        let expected = Block {
            name: "test".to_string(),
            body: vec![Statement::Expression(Expression::integer(20))],
        };
        print_iresult(&result, Some(&expected));
        assert_eq!(result, IResult::Done("".as_bytes(), expected))
    }

    #[test]
    fn multi_statement() {
        assert_eq!(parse_block("test{20 30}".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Block {
                                     name: "test".to_string(),
                                     body: vec![Statement::Expression(Expression::integer(20)),
                                                Statement::Expression(Expression::integer(30))],
                                 }))
    }

    #[test]
    fn whitespace() {
        assert_eq!(parse_block("test {}".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Block {
                                     name: "test".to_string(),
                                     body: vec![],
                                 }))
    }

    #[test]
    fn block_in_block() {
        let input = "
            outer {
                inner {
                    10
                }
            }"
                .as_bytes();

        let expected = Block {
            name: "outer".to_string(),
            body: vec![Statement::Expression(Expression::Block(Block {
                         name: "inner".to_string(),
                         body:
                             vec![Statement::Expression(Expression::integer(10))],
                     }))],
        };

        let result = parse_block(input);

        print_iresult(&result, Some(&expected));

        assert_eq!(result, IResult::Done("".as_bytes(), expected));
    }

    #[test]
    fn multi_line() {
        let input = "
            test {
                10
                2.0
            }"
                .as_bytes();

        let expected = Block {
            name: "test".to_string(),
            body: vec![Statement::Expression(Expression::integer(10)),
                       Statement::Expression(Expression::float(2.0))],
        };

        let result = parse_block(input);

        print_iresult(&result, Some(&expected));

        assert_eq!(parse_block(input), IResult::Done("".as_bytes(), expected));
    }
}

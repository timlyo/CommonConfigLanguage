//! All expresions within the language, used in the config and schema

use std::str;
use compiler::config::statements::Constant;

mod function;
mod list;
mod block;
mod literals;
mod variable;

pub use self::function::{parse_function, Function};
pub use self::list::{parse_list, List};
pub use self::block::{parse_block, Block};
pub use self::literals::{parse_literal, Literal};
pub use self::variable::{parse_variable, Variable};

// an expression is any statement that returns a value and affects the output

/// Has an output that can be evaluated as a string and used
pub trait HasOutput {
    fn get_output(&self) -> String;
}

/// Trait for any statement that stores statements
/// Examples are list and block
pub trait Recursive {
    fn get_constants(&self) -> Vec<Constant>;
}

/// Enum of all possible expressions
#[derive(Debug, PartialEq, Clone)]
pub enum Expression {
    Literal(Literal),
    Block(Block),
    List(List),
    Function(Function),
    Variable(Variable),
}

impl HasOutput for Expression {
    fn get_output(&self) -> String {
        match *self {
            Expression::Literal(ref literal) => literal.to_string(),
            _ => unimplemented!(),
        }
    }
}

impl Expression {
    // Convenience functions

    pub fn integer(value: i64) -> Expression {
        Expression::Literal(Literal::Integer(value))
    }

    pub fn float(value: f64) -> Expression {
        Expression::Literal(Literal::Float(value))
    }

    pub fn string(value: &str) -> Expression {
        Expression::Literal(Literal::String(value.to_string()))
    }
}

named!(pub parse_expression <&[u8], Expression>,
    alt_complete!(
        parse_literal  => {|i| Expression::Literal(i)} |
        parse_block    => {|i| Expression::Block(i)} |
        parse_list     => {|i| Expression::List(i)} |
        parse_function => {|i| Expression::Function(i)} |
        parse_variable => {|i| Expression::Variable(i)}
    )
);

#[cfg(test)]
mod test_expression {
    use super::*;
    use nom::*;

    #[test]
    fn literal_int() {
        assert_eq!(parse_expression("10".as_bytes()),
                   IResult::Done("".as_bytes(), Expression::integer(10)));
    }

    #[test]
    fn literal_float() {
        assert_eq!(parse_expression("1.0".as_bytes()),
                   IResult::Done("".as_bytes(), Expression::float(1.0)));
    }

    #[test]
    fn literal_string() {
        assert_eq!(parse_expression("'10'".as_bytes()),
                   IResult::Done("".as_bytes(), Expression::string("10")));
    }

    #[test]
    fn function() {
        assert_eq!(parse_expression("bort()".as_bytes()),
                   IResult::Done("".as_bytes(),
                                 Expression::Function(Function {
                                                          name: "bort".to_string(),
                                                          parameters: vec![],
                                                      })));
    }

    #[test]
    fn variable() {
        assert_eq!(parse_expression("x = 'thing'\n".as_bytes()),
                   IResult::Done("\n".as_bytes(),
                                 Expression::Variable(Variable::from_str("x", "'thing'").unwrap())));
    }

    #[test]
    fn doesnt_consume_bracket() {
        assert_eq!(parse_expression(")".as_bytes()),
                   IResult::Error(ErrorKind::Alt));
    }

    #[test]
    fn doesnt_consume_curly_bracket() {
        assert_eq!(parse_expression("}".as_bytes()),
                   IResult::Error(ErrorKind::Alt));
    }
}


#[derive(Debug, PartialEq, Clone)]
pub struct Match {
    test: Expression,
    branches: Vec<(Pattern, Expression)>,
}

#[derive(Debug, PartialEq, Clone)]
pub struct Pattern {
    pattern: String,
}

//! parser functions that are commonly used but aren't domain specific

use std::str;
use nom::*;

/// Element name. Starts with a letter and followed with any alphanumeric character
named!(pub identifier<&[u8], String>,
    do_parse!(
        start:
            map_res!(
                verify!(take!(1), |i: &[u8]| is_alphabetic(i[0])), 
                str::from_utf8
            ) >>
        remainder:
            map_res!(
                take_while!(is_valid_identifier_character),
                str::from_utf8
            ) >>

        (
            format!("{}{}", start, remainder)
        )
    )
);

fn is_valid_identifier_character(c: u8) -> bool {
    match c as char {
        'a'...'z' => true,
        'A'...'Z' => true,
        '0'...'9' => true,
        '_' | '-' => true,
        _ => false,
    }
}


#[cfg(test)]
mod test_identifier {
    use super::*;

    #[test]
    fn test_characters() {
        assert_eq!(identifier("test".as_bytes()),
                   IResult::Done("".as_bytes(), "test".to_string()))
    }

    #[test]
    fn test_alpha() {
        assert_eq!(identifier("ab123".as_bytes()),
                   IResult::Done("".as_bytes(), "ab123".to_string()))
    }

    #[test]
    fn test_underscores() {
        assert_eq!(identifier("a_123".as_bytes()),
                   IResult::Done("".as_bytes(), "a_123".to_string()))
    }

    #[test]
    fn test_id_character() {
        assert!(is_valid_identifier_character('a' as u8));
        assert!(is_valid_identifier_character('t' as u8));
        assert!(is_valid_identifier_character('q' as u8));
        assert!(is_valid_identifier_character('z' as u8));
        assert!(is_valid_identifier_character('R' as u8));
        assert!(is_valid_identifier_character('T' as u8));
        assert!(is_valid_identifier_character('_' as u8));
        assert!(is_valid_identifier_character('-' as u8));
    }
}

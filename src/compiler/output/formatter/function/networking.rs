use super::*;
use std::net::{IpAddr, Ipv4Addr, Ipv6Addr};
use std::str::FromStr;

pub fn valid_ip(parameters: &Vec<Expression>) -> Result<String, Vec<CompilerError>> {
    if parameters.len() != 1 {
        let message = format!("Expected 1 parameter, found {:?}", parameters);
        return Err(vec![CompilerError::parameter_error(&message, "valid_ipv4 function")]);
    }

    let ip = parameters.first().unwrap().get_output();
    if IpAddr::from_str(&ip).is_ok() {
        Ok(ip.to_string())
    } else {
        let message = format!("{} is not a valid ipv4 address", ip);
        Err(vec![CompilerError::parameter_error(&message, "valid_ipv4 function")])
    }
}

pub fn valid_ipv4(parameters: &Vec<Expression>) -> Result<String, Vec<CompilerError>> {
    if parameters.len() != 1 {
        let message = format!("Expected 1 parameter, found {:?}", parameters);
        return Err(vec![CompilerError::parameter_error(&message, "valid_ipv4 function")]);
    }

    let ip = parameters.first().unwrap().get_output();
    if Ipv4Addr::from_str(&ip).is_ok() {
        Ok(ip.to_string())
    } else {
        let message = format!("{} is not a valid ipv4 address", ip);
        Err(vec![CompilerError::parameter_error(&message, "valid_ipv4 function")])
    }
}

pub fn valid_ipv6(parameters: &Vec<Expression>) -> Result<String, Vec<CompilerError>> {
    if parameters.len() != 1 {
        let message = format!("Expected 1 parameter, found {:?}", parameters);
        return Err(vec![CompilerError::parameter_error(&message, "valid_ipv4 function")]);
    }

    let ip = parameters.first().unwrap().get_output();
    if Ipv6Addr::from_str(&ip).is_ok() {
        Ok(ip.to_string())
    } else {
        let message = format!("{} is not a valid ipv4 address", ip);
        Err(vec![CompilerError::parameter_error(&message, "valid_ipv4 function")])
    }
}

pub fn reachable_ip(parameters: &Vec<Expression>) -> Result<String, Vec<CompilerError>> {
    unimplemented!()
}

pub fn valid_url(parameters: &Vec<Expression>) -> Result<String, Vec<CompilerError>> {
    unimplemented!()
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_ip() {
        assert!(valid_ip(&vec![Expression::string("192.168.0.1")]).is_ok());
        assert!(valid_ipv4(&vec![Expression::string("192.168.0.1")]).is_ok());
        assert!(valid_ipv6(&vec![Expression::string("2001:0db8:85a3:0000:0000:8a2e:0370:7334")])
                    .is_ok());

        assert!(valid_ip(&vec![Expression::string("999.168.0.1")]).is_err());
        assert!(valid_ipv4(&vec![Expression::string(".168.0.1")]).is_err());
        assert!(valid_ipv6(&vec![Expression::string("2001:0db8:85a3:0000:0000:8a2e:0370")])
                    .is_err());
    }
}

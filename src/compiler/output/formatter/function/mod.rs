use compiler::expressions::*;
use compiler::schema::Schema;
use compiler::errors::CompilerError;

mod networking;

pub fn format_function(function: &Function, schema: &Schema) -> Result<String, Vec<CompilerError>> {
    let parameters = &function.parameters;
    match function.name.as_str() {
        "existing_directory" => existing_directory(parameters),
        "valid_ip" => networking::valid_ip(parameters),
        "valid_ipv4" => networking::valid_ipv4(parameters),
        "valid_ipv6" => networking::valid_ipv6(parameters),
        _ => {
            Err(vec![CompilerError::unrecognised_error(&format!("function: {}", function.name),
                                                  "function call")])
        }
    }
}


/// Checks if a directory exists
fn existing_directory(parameters: &Vec<Expression>) -> Result<String, Vec<CompilerError>> {
    use std::path::Path;

    if parameters.len() != 1 {
        let message = format!("only expected 1 parameter found {:?}", parameters);
        return Err(vec![CompilerError::parameter_error(&message, "existing_directory function")]);
    }

    let path = parameters.first().unwrap().get_output();

    if Path::new(&path).exists() {
        Ok(path)
    } else {
        let message = format!("A directory called {} cannot be found", path);
        Err(vec![CompilerError::logic_error(&message, "existing_directory function")])
    }
}


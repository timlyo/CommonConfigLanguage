use compiler::expressions::Expression;
use compiler::schema::Schema;
use compiler::errors::CompilerError;
use compiler::output::formatter::*;

pub fn format_expression(expression: &Expression,
                         schema: &Schema)
                         -> Result<String, Vec<CompilerError>> {
    match *expression {
        Expression::Literal(ref literal) => Ok(literal.to_string()),
        Expression::Block(ref block) => format_block(block, schema),
        Expression::List(ref list) => format_list(list, schema),
        Expression::Function(ref function) => format_function(function, schema),
        Expression::Variable(ref variable) => format_key_value(variable, schema),
    }
}

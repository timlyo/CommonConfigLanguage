use compiler::expressions::List;
use compiler::schema::Schema;
use compiler::expressions::HasOutput;
use compiler::errors::CompilerError;

use compiler::output::formatter::format_expression;

pub fn format_list(list: &List, schema: &Schema) -> Result<String, Vec<CompilerError>> {

    let list_trait = schema.get_lists_trait();

    if list_trait.is_none() {
        println!("Warning: formatting list with no list trait defined");
        match list.first() { // TODO consider alternative forms of output
            Some(ref elem) => return format_expression(elem, schema),
            None => return Ok("".to_string()),
        }
    }

    let list_trait = list_trait.unwrap();

    let body = list.data
        .iter()
        .map(|x| x.get_output())
        .collect::<Vec<String>>()
        .as_slice()
        .join(&list_trait.separator);

    Ok(format!("{}{}{}",
               list_trait.start_delimiter,
               body,
               list_trait.end_delimiter))
}

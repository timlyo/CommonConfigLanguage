use compiler::config::statements::Statement;
use compiler::schema::Schema;
use compiler::errors::CompilerError;
use compiler::output::formatter::format_expression;

pub fn format_statement(statement: &Statement,
                        schema: &Schema)
                        -> Result<String, Vec<CompilerError>> {
    let body = match *statement {
        Statement::Expression(ref expression) => format_expression(expression, schema),
        Statement::Constant(_) => return Ok("".to_string()),
    };

    // unwrap body and return errors
    let body = match body {
        Ok(body) => body,
        Err(e) => return Err(e),
    };

    if let Some(line_endings_trait) = schema.get_line_endings_trait() {
        Ok(body + &line_endings_trait.terminator)
    } else {
        Ok(body)
    }
}

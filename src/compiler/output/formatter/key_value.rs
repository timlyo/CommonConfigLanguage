use compiler::expressions::Variable;
use compiler::schema::Schema;
use compiler::errors::CompilerError;

use compiler::output::formatter::format_expression;

pub fn format_key_value(variable: &Variable,
                        schema: &Schema)
                        -> Result<String, Vec<CompilerError>> {
    let key_value_trait = match schema.get_key_value_trait() {
        None => return Ok("".to_string()),
        Some(t) => t,
    };

    let value = try!(format_expression(&variable.value, schema));
    //let name = rt_format!(key_value_trait.name_format, name=variable.name);

    Ok(format!("{name}{separator}{value}",
               name = variable.name,
               separator = key_value_trait.separator,
               value = value))
}

#[cfg(test)]
mod test {
    use super::*;
    use compiler::schema::parse::parse_schema;

    #[test]
    fn test_format() {
        let schema = parse_schema("\
# Variables
# Traits
key_value(separator=':', name_format='{name}')"
                                          .as_bytes());
        let schema = schema.to_result().unwrap();


        let variable = Variable::from_str("x", "10").unwrap();
        let result = format_key_value(&variable, &schema);
        assert_eq!(result, Ok("x:10".to_string()))
    }
}

use compiler::expressions::Block;
use compiler::schema::Schema;
use compiler::errors::CompilerError;

use compiler::output::formatter::format_statement;

pub fn format_block(block: &Block, schema: &Schema) -> Result<String, Vec<CompilerError>> {
    let mut statements = block
        .body
        .iter()
        .map(|statement| format_statement(statement, schema))
        .collect::<Vec<Result<String, Vec<CompilerError>>>>();

    //TODO change to partition
    // return any errors
    let errors: Vec<CompilerError> = statements
        .clone()
        .into_iter()
        .filter_map(|x| match x {
                        Ok(_) => None,
                        Err(x) => Some(x),
                    })
        .flat_map(|x| x)
        .collect();

    if !errors.is_empty() {
        return Err(errors);
    }

    let body = statements
        .into_iter()
        .filter(|x| x.is_ok())
        .map(|x| x.unwrap())
        .collect::<String>();


    let block_trait = schema.get_block_trait();

    //TODO consider outputting block name as a comment if not supported
    if block_trait.is_none() {
        return Ok(body);
    }


    let block_trait = block_trait.unwrap();
    let formatted_name = block_trait.name_format.replace("{name}", &block.name);

    Ok(format!("{prefix}{name}{s}{body}{e}",
               prefix = block_trait.prefix,
               s = block_trait.start_delimiter,
               name = formatted_name,
               e = block_trait.end_delimiter,
               body = body))
}

//! functions pertaining to output generation

use compiler::config::statements::Statement;
use compiler::schema::Schema;
use compiler::errors::CompilerError;

pub mod formatter;

/// Recursivley evaluate statements to `Strings`
pub fn get_output(input: Vec<Statement>, schema: Schema) -> Result<String, Vec<CompilerError>> {
    let output = input
        .iter()
        .map(|statement| formatter::format_statement(statement, &schema))
        .collect::<Vec<Result<String, Vec<CompilerError>>>>();

    let errors = output
        .clone()
        .into_iter()
        .filter_map(|x| match x {
                        Err(x) => Some(x), // Keep errors
                        Ok(_) => None, // Discard Strings
                    })
        .flat_map(|x| x)
        .collect::<Vec<CompilerError>>();

    if errors.is_empty() {
        // No Errors so return
        Ok(output
               .into_iter()
               .filter_map(|x| match x { 
                               Ok(x) => Some(x),
                               Err(e) => None,
                           })
               .collect::<String>())
    } else {
        Err(errors)
    }
}

#[cfg(test)]
mod test_parse {
    use super::*;
    use compiler::config::parse_config;
    use compiler::schema::parse_schema;

    #[test]
    fn test_simple() {
        let input = parse_config("x = 10".as_bytes());
        println!("Input {:?}", input);
        assert!(input.is_done());
        let input = input.to_result().unwrap();

        let schema = parse_schema("\
# Variables
# Traits
key_value(separator='-', name_format='{name}')
"
                                          .as_bytes());
        println!("Schema {:?}", schema);
        assert!(schema.is_done());
        let schema = schema.unwrap().1;

        let output = get_output(input, schema);
        assert_eq!(output, Ok("x-10".to_string()));
    }
}

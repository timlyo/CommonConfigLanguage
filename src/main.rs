#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]

#![cfg_attr(test, feature(plugin))]
#![cfg_attr(test, plugin(quickcheck_macros))]

#[cfg(test)] extern crate quickcheck;
#[macro_use] extern crate runtime_fmt;
#[macro_use] extern crate nom;
extern crate clap;

pub mod compiler;
pub mod system;

use clap::{Arg, App};

use std::path::Path;

pub fn main() {
    let matches = App::new("Ccl")
        .arg(Arg::with_name("config")
                 .help("Input config")
                 .required(true))
        .arg(Arg::with_name("schema")
                 .help("Format schema")
                 .required(true))
        .arg(Arg::with_name("output")
                 .short("-o")
                 .long("--output")
                 .help("Which file to output to, defaults to stdout")
                 .value_name("output file"))
        .get_matches();

    // config and schema are marked as required above so panic is valid
    let config_name = matches.value_of("config").unwrap();
    let schema_name = matches.value_of("schema").unwrap();

    let config = system::get_config_contents(Path::new(config_name));
    let config = compiler::parse_config(config.as_bytes())
        .to_result()
        .unwrap();

    println!("Config");
    println!("{:?}", config);
    println!();

    let schema = system::get_schema_contents(Path::new(schema_name));
    let schema = compiler::parse_schema(schema.as_bytes())
        .to_result()
        .unwrap();

    println!("{:#?}", schema);
    println!();

    match compiler::get_output(config, schema){
        Ok(output) => println!("output: {}", output),
        Err(errors) => {
            for error in errors{
                println!("{:?}", error);
            }
        }
    }
}

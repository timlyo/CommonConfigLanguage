use std::fs::File;
use std::path::Path;
use std::io::Read;

pub fn get_config_contents(path: &Path) -> String {
    let mut buf = String::new();
    let _ = match File::open(path) {
        Ok(mut file) => file.read_to_string(&mut buf),
        Err(e) => {
            println!("{}", e);
            panic!(e);
        }
    };

    buf
}

pub fn get_schema_contents(path: &Path) -> String {
    // TODO search system directories if path is not relative
    let mut buf = String::new();
    let _ = match File::open(path) {
        Ok(mut file) => file.read_to_string(&mut buf),
        Err(e) => {
            println!("Error opening {:?}", path);
            println!("{}", e);
            panic!(e);
        }
    };

    buf
}

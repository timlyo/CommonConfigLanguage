# Parsing

Converts a config into a list or statements and a schema into a Schema struct

# Verification

## Name checking

Checks that all names are valid, this includes function, trait

## Type checking

Checks that all function parameters have valid types

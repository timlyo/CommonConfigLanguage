# Config specific

## i3

- System functions
	- i.e. `systemfunction param1 param2`
- variables
- blocks

## bashrc

- All of bash

## Nginx

- Key value(" ", multi)
	- example of multi: `error_log  /var/log/nginx/error.log warn;`
- blocks(false, , ["{", "}"])
- Line endings(";")

## Uwsgi

- ini file

# General Configs

## Ini

- Key value("=")
- Blocks(true, "[%n]", false)

## Json

- blocks(false, , ["{", "}"])
- key value(":")
- lists(["[", "]"], ",")

- constraints
	- Single root element, either block or list
	- All key values must be inside a block 

## Yaml

Only the short form of YAML is generated, the result can be run through a prettyfier later if required in another form. Much of the YAML spec is unused because most of it is for making the text human readable which isn't required for the output of the project.

- blocks(true, ,["{". "}"])
- key value(":")
- lists(["[", "]"], ,)

## XML

## TOML

# All traits

## Key-value

- separator
- single or multi value

## Blocks

Contain everything within them in a scope

- named
- Name format
- delimiter

## System functions

- syntax

## User functions

## Line Endings

- end character

## Lists

- delimiters
- separator

## Comments

The language will have two types of comments `#` for ccl comments and `#!` for comments that end up in the output

- Start character
- End character
- Multiline


# Error codes

## 100 - Syntax Error 

101 - missing indent
102 - missing EOL
103 - missing variable list
104 - missing trait list

## 200 - Logic Error

201 - Trait not recognised
202 - invalid trait parameter

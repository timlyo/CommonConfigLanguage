[![build status](https://gitlab.com/timlyo/CommonConfigLanguage/badges/master/build.svg)](https://gitlab.com/timlyo/CommonConfigLanguage/commits/master)

The Common Config Language is a project aiming to create a simple language that can be compiled into the syntax of as many configuration languages as possible, whilst statically analysing the input to reduce the scope of possible mistakes.

The Trello board for tracking the project is available [here] (https://trello.com/b/z9RBmv0t/common-config-language)